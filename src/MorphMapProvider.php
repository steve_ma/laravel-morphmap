<?php

namespace Stevema\MorphMap;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class MorphMapProvider extends ServiceProvider
{
    public mixed $resource = null;
    /**
     * Register services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        // 发布配置文件
        $this->initPublishes();
        // 合并配置
        $this->mergeConfigFrom(
            realpath(__DIR__.'/Config/config.php'), 'morphmap'
        );
        // 注册命令
        $this->registerCommands();

        $resource = new MorphConfigResource();
        $this->resource = $resource;
        // 动态注册模型别名
        Relation::enforceMorphMap($resource->getAlias());
        // 动态注册多态关系
        $this->resolveRelationUsings();
    }

    protected function initPublishes(){
        // 发布配置文件
        if ($this->app->runningInConsole()) {
            $this->publishes([
                realpath(__DIR__.'/Config/config.php') => config_path('morphmap.php'),
            ],'morphmap');
        }
    }

    protected function registerCommands(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Stevema\MorphMap\Consoles\Commands\MorphTableCommand::class,
                \Stevema\MorphMap\Consoles\Commands\MorphListCommand::class,
            ]);
        }
    }

    public function resolveRelationUsings(): void
    {
        $resource = $this->resource;
        $maps = $resource->getMaps();
        if(!empty($maps)){
            foreach( $maps as $k => $morph) {
                $modelClass = $morph['model'];
                $morphs = $morph['morphs'];
                if($morph['is_morph']) {
                    // 有主体 有关系则开始
                    if (!empty($morphs)) {
                        if ($resource->isMORPH_Many2Many($morph['type'])) {
                            //多对多
                            foreach ($morphs as $ableClass => $ableName) {
                                //able关联主体
                                $this->bindMorphToMany($ableClass, $morph['morph_name'], $modelClass, $morph);
                                //主体 关联 able
                                $this->bindMorphByMany($modelClass, $ableName, $ableClass, $morph);
                            }
                        } else {
                            // 一对一 一对多 主体关联able
                            $this->bindMorphTo($modelClass, $morph['morphs_name'], $morph);
                            // able关联主体
                            foreach ($morphs as $ableClass => $ableName) {
                                if ($resource->isMORPH_ONE2ONE($morph['type'])) {
                                    $this->bindMorphOne($ableClass, $ableName, $morph);
                                }
                                if ($resource->isMORPH_ONE2MANY($morph['type'])) {
                                    $this->bindMorphMany($ableClass, $ableName, $morph);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function bindMorphTo($class, $ableName, $morph){
        if(class_exists($class)) {
            $class::resolveRelationUsing($ableName, function ($model) use ($morph) {
                return $model->morphTo(
                    $morph['able_name'], // able
                    $morph['able_type_name'], // able_type
                    $morph['able_id_name'], // able_id
                    $morph['primarys'][get_class($model)] // ownerKey  当前模型的主键
                );
            });
        }
    }
    public function bindMorphOne($class, $ableName, $morph){
        if(class_exists($class)) {
            $class::resolveRelationUsing($ableName, function ($model) use ($morph) {
                //$related, $name, $type = null, $id = null, $localKey = null
                return $model->morphOne(
                    $morph['model'],    // 关联模型
                    $morph['able_name'], // able
                    $morph['able_type_name'], // able_type
                    $morph['able_id_name'], // able_id
                    $morph['primarys'][get_class($model)] // localKey  当前模型的主键
                );
            });
        }
    }
    public function bindMorphMany($class, $ableName, $morph){
        if(class_exists($class)) {
            $class::resolveRelationUsing($ableName, function ($model) use ($morph) {
                //$related, $name, $type = null, $id = null, $localKey = null
                return $model->morphMany(
                    $morph['model'],    // 关联模型
                    $morph['able_name'], // able
                    $morph['able_type_name'], // able_type
                    $morph['able_id_name'], // able_id
                    $morph['primarys'][get_class($model)] // localKey  当前模型的主键
                );
            });
        }
    }
    public function bindMorphToMany($class, $ableName, $ableClass, $morph){
        if(class_exists($class) && class_exists($ableClass)) {
            $class::resolveRelationUsing($ableName, function ($model) use ($morph, $ableClass) {
                //$related, $name, $table = null, $foreignPivotKey = null,
                //  $relatedPivotKey = null, $parentKey = null,
                //  $relatedKey = null, $relation = null, $inverse = false
                return $model->morphToMany(
                    $ableClass,    // 关联模型
                    $morph['able_name'], // 中间表的 able
                    $morph['table_name'], // 中间表名
                    $morph['able_id_name'], // foreignPivotKey 中间表的 关联当前模型对应字段
                    $morph['primary_name'], // relatedPivotKey 中间表的 主体键
                    $morph['primarys'][get_class($model)], // parentKey  当前模型的主键
                    $morph['primarys'][$ableClass] // relatedKey 关联模型的主键
                );
            });
        }
    }
    public function bindMorphByMany($class, $ableName, $ableClass, $morph){
        if(class_exists($class) && class_exists($ableClass)) {
            $class::resolveRelationUsing($ableName, function ($model) use ($morph, $ableClass) {
                //$related, $name, $table = null, $foreignPivotKey = null,
                //  $relatedPivotKey = null, $parentKey = null, $relatedKey = null, $relation = null
                return $model->morphedByMany(
                    $ableClass,
                    $morph['able_name'], // 中间表的 able
                    $morph['table_name'], // 中间表名
                    $morph['primary_name'], // foreignPivotKey 中间表的 关联当前模型对应字段
                    $morph['able_id_name'], // relatedPivotKey 中间表的 主体键
                    $morph['primarys'][get_class($model)], // parentKey  当前模型的主键
                    $morph['primarys'][$ableClass] // relatedKey 关联模型的主键
                );
            });
        }
    }

}
