<?php


namespace Stevema\MorphMap\Consoles\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Stevema\MorphMap\MorphConfigResource;

class MorphTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'morph:table {name : table Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create morph table migrate';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var string $name */
        $name = $this->argument("name");

        $resource = new MorphConfigResource();
        $morph = $resource->getMapOne($name);
        if(is_null($morph)){
            $this->error("{$name} 的配置有些许的问题!");
            die;
        }

        $will_path = database_path("migrations/2023_08_24_000001_create_{$name}_table.php");
        $stub_path = realpath(__DIR__ . '/../Stubs/'.$morph['type'].'.stub');
        if (!File::exists($will_path)) {
            if (!File::isDirectory(File::dirname($will_path))) {
                File::makeDirectory(File::dirname($will_path), 493, true);
            }
            File::put(
                $will_path,
                str_replace(
                    [
                        '{name}', '{comment}',
                        '{primary_name}',
                        '{able_type_name}', '{able_id_name}',
                    ],
                    [
                        $name, $morph['comment'],
                        $morph['primary_name'],
                        $morph['able_type_name'],$morph['able_id_name'],
                    ],
                    File::get($stub_path)
                )
            );
            $this->components->info(sprintf('%s [%s] created successfully.', 'migrate', $will_path));
        } else {
            $this->components->error(sprintf('%s [%s] already exists.', 'migrate', $will_path));
        }

        return 0;

    }
}
