<?php
namespace Stevema\MorphMap\Consoles\Commands;

use Illuminate\Console\Command;
use Stevema\MorphMap\MorphConfigResource;

class MorphListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'morph:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create morph table migrate';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $resource = new MorphConfigResource();
        $maps = $resource->getMaps();
        $arr = [];
        if(!empty($maps)){
            foreach( $maps as $k => $morph) {
                $modelClass = $morph['model'];
                $morphs = $morph['morphs'];
                $morph_type_comment = "[{$resource->getTypeComment($morph['type'])}]";
                // 有主体 有关系则开始
                if(!empty($morphs) && class_exists($modelClass)) {
                    if(!$resource->isMORPH_Many2Many($morph['type'])){
                        // 一对一 一对多
                        $arr[$modelClass][] = [
                            'name' => $morph['morphs_name'],
//                            'comment' => $morph['comment'],
                            'comment' => $morph_type_comment.'查看我的'.$morph['comment'].'关联模型',
                            'master' => '是',
                        ];
                    }
                    foreach ($morphs as $ableClass => $ableName) {
                        //able关联主体
                        if(class_exists($ableClass)) {
                            $arr[$ableClass][] = [
                                'name' => $morph['morph_name'],
//                                'comment' => $morph['comment'],
                                'comment' => $morph_type_comment.'查看我的'.$morph['comment'],
                                'master' => '否',
                            ];
                        }
                        //主体 关联 able
                        if($resource->isMORPH_Many2Many($morph['type'])){
                            if(class_exists($modelClass)) {
                                $arr[$modelClass][] = [
                                    'name' => $ableName,
//                                    'comment' => $morph['comment'],
                                    'comment' => $morph_type_comment.'查看我和关联模型的'.$morph['comment'],
                                    'master' => '是',
                                ];
                            }
                        }
                    }
                }
            }
        }

        $headers = ['模型', '关联方法', '备注', '关联主体'];
        $orders = [];
        foreach($arr as $class => $relations){
            if(empty($relations) === FALSE) {
                foreach($relations as $key => $relation){
                    $orders[] = [
                        $class, $relation['name'],$relation['comment'],$relation['master'],
                    ];
                }
            }
        }
        $this->table($headers, $orders);

        return 0;

    }
}
