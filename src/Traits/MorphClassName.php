<?php
namespace Stevema\MorphMap\Traits;
use Illuminate\Database\Eloquent\Relations\Relation;
// 多态关联的时候 模型 use这个可以自定义 able_type 的 别名
trait MorphClassName
{
    // 配置多态的关联关系后 其中的able_type竟然是 App\Models\User 这种
    // 很长不说 还把我的命名空间给暴露了 - 不能忍

    // 没配置别名的话 使用表明当做别名
    public bool $useTableNameToMorphName = false;

    // 如果没配置别名还不想用表名的话 可以分割命名空间直到获取到模型名
    public bool $classBasename = True;

    // 模型都会有这个方法 我这里重写它 按我想要的规则去找别名
    public function getMorphClass()
    {
        // 获取别名
        $morphMap = Relation::morphMap();
        // 查找别名 找到就返回
        if (! empty($morphMap) && in_array(static::class, $morphMap)) {
            return array_search(static::class, $morphMap, true);
        }
        // 输出表名
        if($this->useTableNameToMorphName){
            return $this->getTable();
        }
        // 输出类名 去掉命名空间后的
        if($this->classBasename){
            return class_basename(self::class);
        }
        // 最不能忍的就是这个输出带命名空间的类名
        return self::class;
    }
}
