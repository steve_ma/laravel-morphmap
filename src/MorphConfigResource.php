<?php

namespace Stevema\MorphMap;
// 配置解释器
//use Illuminate\Database\Eloquent\Model;

class MorphConfigResource {
    public mixed $config = null;

    public const ONE2ONE = 'one2one';
    public const ONE2ONE_COMMENT = '多态一对一';
    public const ONE2MANY = 'one2many';
    public const ONE2MANY_COMMENT = '多态一对多';
    public const MANY2MANY = 'many2many';
    public const MANY2MANY_COMMENT = '多态多对多';

    public const MORPH_ONE2ONE = 'morph_one2one';
    public const MORPH_ONE2ONE_COMMENT = '多态一对一';
    public const MORPH_ONE2MANY = 'morph_one2many';
    public const MORPH_ONE2MANY_COMMENT = '多态一对多';
    public const MORPH_MANY2MANY = 'morph_many2many';
    public const MORPH_MANY2MANY_COMMENT = '多态多对多';

    public function __construct()
    {
        $this->config = app('config')->get('morphmap');
    }

    public function getAlias(){
        return $this->config['alias'];
    }

    public function getMaps(): array
    {
        $maps = [];
        $morphs = $this->config['morphs'];
        foreach($morphs as $table_name => $morph){
            $morph = $this->getFullConfig($table_name, $morph);
            if(!is_null($morph)){
                $maps[] = $morph;
            }
        }
        return $maps;
    }
    public function getMapOne($name){
        $morphs = $this->config['morphs'];
        if(isset($morphs[$name])){
            return $this->getFullConfig($name, $morphs[$name], 1);
        }
        return null;
    }

    public function checkType($type): bool
    {
        if(in_array($type, [
            static::MORPH_ONE2ONE,
            static::MORPH_ONE2MANY,
            static::MORPH_MANY2MANY,
        ])){
            return True;
        }
        return false;
    }
    public function isMorph($type): bool
    {
        if(in_array($type, [
            static::MORPH_ONE2ONE,
            static::MORPH_ONE2MANY,
            static::MORPH_MANY2MANY,
        ])){
            return True;
        }
        return false;
    }
    public function isMORPH_Many2Many($type): bool
    {
        return static::MORPH_MANY2MANY == $type;
    }
    public function isMORPH_ONE2MANY($type): bool
    {
        return static::MORPH_ONE2MANY == $type;
    }
    public function isMORPH_ONE2ONE($type): bool
    {
        return static::MORPH_ONE2ONE == $type;
    }
    public function getTypeComment($type): ?string
    {
        if($this->isMORPH_ONE2ONE($type)) return static::MORPH_ONE2ONE_COMMENT;
        if($this->isMORPH_ONE2MANY($type)) return static::MORPH_ONE2MANY_COMMENT;
        if($this->isMORPH_Many2Many($type)) return static::MORPH_MANY2MANY_COMMENT;
        return null;
    }


    public function getClass($name){
        //检查别名里面有没有
        $alias = $this->config['alias'];
        if(isset($alias[$name])){
            return $alias[$name];
        }
        return $name;
    }
    public function getPrimaryKey($name){
        //检查别名里面有没有 primarys 还是关键词不让用
        $primaries = $this->config['primarys'];
        $classname = $this->getClass($name);
        if(isset($primaries[$classname])){
            return $primaries[$classname];
        }
        return 'id';
    }
    public function checkModelClass($name): bool
    {
//        assert($name instanceof Model);
        return class_exists($name);
    }
    public function getFullConfig($table_name, $morph, $noModel = 0){
        $morph['table_name'] = $table_name;
        if(!$this->checkType($morph['type'])) return null;
        $morph['is_morph'] = $this->isMorph($morph['type']);
        if($this->isMorph($morph['type'])){
            return $this->getMorphFull($morph, $noModel);
        } else {
            return $this->getFull($morph, $noModel);
        }
    }
    public function getFull($morph, $noModel=0){
        return $morph;
    }
    public function getMorphFull($morph, $noModel=0){
        $table_name = $morph['table_name'];
        if(!isset($morph['comment'])) $morph['comment'] = null;
        if(!isset($morph['model'])) $morph['model'] = null;
        if(!isset($morph['primary_name'])) $morph['primary_name'] = null;
        if(!isset($morph['able_name'])) $morph['able_name'] = null;
        if(!isset($morph['able_type_name'])) $morph['able_type_name'] = null;
        if(!isset($morph['able_id_name'])) $morph['able_id_name'] = null;
        if(!isset($morph['morphs_name'])) $morph['morphs_name'] = null;
        if(!isset($morph['morph_name'])) $morph['morph_name'] = null;
        if(!isset($morph['morphs'])) $morph['morphs'] = [];
        // 主体模型 输出 user => App\Models\User
        $modelClass = $this->getClass($morph['model']);
        // 主键集合
        $morph['primarys'] = [];

        // 主体模型为空 则 整个关系不存在
        if(!$this->checkModelClass($modelClass) && !$noModel) return null;
        if(empty($modelClass)){
            # 表是不是以s结尾的  如果是去掉
            if(str_ends_with($modelClass, 's')){
                $modelClass = substr($table_name, 0, -1);
            } else {
                $modelClass = $table_name;
            }
        };
        $morph['model'] = $modelClass;

        // 主体模型的主键
        $morph['primarys'][$modelClass] = $this->getPrimaryKey($modelClass);
        // 获取主体模型的模型名称 App\Models\User => user
        $modelName = strtolower(class_basename($modelClass));
        // 主体模型的主键 primary_name
        if(empty($morph['primary_name'])) $morph['primary_name'] = $modelName."_".$this->getPrimaryKey($modelClass);
        // 关联关系的able  -> userable
        if(empty($morph['able_name'])) $morph['able_name'] = $modelName."able";
        // able_type  able_id
        if(empty($morph['able_type_name'])) $morph['able_type_name'] = $morph['able_name']."_type";
        if(empty($morph['able_id_name'])) $morph['able_id_name'] = $morph['able_name']."_id";
        // 主体中定义的的方法名  如果为空则为 able_name的值 多对多关系这个就不需要了
        if(empty($morph['morphs_name'])) $morph['morphs_name'] = $modelName."able";
        // 关系中定义的方法名 为空则用关系表名
        if(empty($morph['morph_name'])) $morph['morph_name'] = $table_name;
        $morphs = $morph['morphs'];
        // 没有关系 也不存在
        if(empty($morphs)) return null;
        foreach($morphs as $ableClass => $ableName){
            # 有用别名的情况下 先删除掉
            unset($morph['morphs'][$ableClass]);
            # 再 重写
            $ableClass = $this->getClass($ableClass);
            if(empty($ableName)) {
                if($this->isMORPH_Many2Many($morph['type'])){
                    // 关系模型绑定 morph_name
                    $ableName = strtolower(class_basename($ableClass)) .'_'. $morph['morph_name'];
                } else {
                    $ableName = $morph['morph_name'];
                }
            }
            $morph['morphs'][$ableClass] = $ableName;
            // 模型的主键
            $morph['primarys'][$ableClass] = $this->getPrimaryKey($ableClass);
        }
        unset($morphs);
        return $morph;
    }
}
