<?php
use Stevema\MorphMap\MorphConfigResource;
return [
    /*
    |--------------------------------------------------------------------------
    | MorphMap Config
    |--------------------------------------------------------------------------
    |
    | 多态关联关系别名 - 具体格式如下
    | 'alias' => [
    |     '别名' => 模型::class,
    |     'user' => \App\Models\User::class
    | ],
    |
    | 主键关系  不定义或者null 主键就是id 所以可以不定义 没有特殊这个可以不定义
    | 'primarys' => [
    |     // '别名/模型:class' => '主键'
    |     // \App\Models\User::class => 'id',
    | ],
    |
    | 'morphs' => [
    |     '关系表名' => [
    |         // 关系表说明 多对多是中间表 一对一和一对多是主体表
    |         'comment' => '关系表说明',
    |         // 关系 MorphConfigResource::MORPH_ONE2ONE[多态一对一]
    |         // 关系 MorphConfigResource::MORPH_ONE2MANY[多态一对多]
    |         // 关系 MorphConfigResource::MORPH_MANY2MANY[多态多对多]
    |         'type' => MorphConfigResource::MORPH_MANY2MANY,
    |         // 一对一 和 一对多 是需要相关的主体模型 多对多也需要但是主体有些不一样
    |         'model' => '别名/模型:class', // 别名或者模型::class
    |         // 主体在关系表中的字段名 多对多关系用到  为空则为 主体模型.主体模型的主键
    |         'primary_name' => null,
    |         // 关联的ablename  为空则默认关系表名.'_able'
    |         'able_name' => 'able',
    |         // able_type_name 字段名 如果null 则自动根据able_name 生成 [$(able_name) . '_type']
    |         'able_type_name' => 'able_type',
    |         // able_id_name 字段名 如果null 则自动根据able_name 生成 [$(able_name) . '_id']
    |         'able_id_name' => 'able_id',
    |         // 主体中定义的的方法名 如果为空则为 关系表名.'_able' 多对多关系这个就不需要了
    |         'morphs_name' => '方法名',
    |         // 关系中定义的方法名 为空则用关系表名
    |         'morph_name' => '方法名',
    |         //关系列表 主体对应多个模型
    |         'morphs' => [
    |              // 别名或者模型:class  比如 Post::class 关联
    |              // '别名/模型:class' => '方法名',
    |              // 一对一 一对多 的情况下 方法名会绑定在关系模型中  null 则用 morph_name 覆盖
    |              // 多对多 的情况下 方法名会绑定在主体模型中  null 则用 关系模型.'_'. morph_name 方法
    |         ],
    |     ],
    | ],
    |
    |
    */

    // 关系别名 多态关系中 有个别名还是有些好处的
    'alias' => [
        // '别名' => 模型::class,
    ],
    // 主键关系  不定义或者null 主键就是id 所以可以不定义 没有特殊这个可以不定义
    'primarys' => [
        // '别名/模型:class' => '主键'
    ],

    // 关联关系 多态关系是需要关联表名的
    'morphs' => [
        //'关系表名' => [
        //    // 关系表说明 多对多是中间表 一对一和一对多是主体表
        //    'comment' => '关系表说明',
        //    // 关系 MorphConfigResource::MORPH_ONE2ONE[多态一对一]
        //    // 关系 MorphConfigResource::MORPH_ONE2MANY[多态一对多]
        //    // 关系 MorphConfigResource::MORPH_MANY2MANY[多态多对多]
        //    'type' => MorphConfigResource::MORPH_ONE2ONE,
        //    // 一对一 和 一对多 是需要相关的主体模型 多对多也需要但是主体有些不一样
        //    'model' => '别名/模型:class', // 别名或者模型::class
        //    // 主体在关系表中的字段名 多对多关系用到  为空则为 主体模型.主体模型的主键
        //    'primary_name' => null,
        //    // 关联的ablename 为空则默认关系表名.'able'
        //    'able_name' => 'able',
        //    // able_type_name 字段名 如果null 则自动根据able_name 生成 [$(able_name) . '_type']
        //    'able_type_name' => 'able_type',
        //    // able_id_name 字段名 如果null 则自动根据able_name 生成 [$(able_name) . '_id']
        //    'able_id_name' => 'able_id',
        //    // 主体中定义的的方法名 如果为空则为 关系表名.'_able' 多对多关系这个就不需要了
        //    'morphs_name' => '方法名',
        //    // 关系中定义的方法名 为空则用关系表名
        //    'morph_name' => '方法名',
        //    //关系列表 主体对应多个模型
        //    'morphs' => [
        //         // 别名或者模型:class  比如 Post::class 关联
        //         // '别名/模型:class' => '方法名',
        //         // 一对一 一对多 的情况下 方法名会绑定在关系模型中  null 则用 morph_name 覆盖
        //         // 多对多 的情况下 方法名会绑定在主体模型中  null 则用 关系模型.'_'. morph_name 方法
        //    ],
        //],

        /**
         * 多态一对一关系 示例
         * 示例表结构
         * posts                  users                 images
         *  id - integer            id - integer            id - integer
         *  name - string           name - string           url - string
         *                                                  imageable_id - integer
         *                                                  imageable_type - string
         * 示例模型结构
         * namespace App\Models;
         * use Illuminate\Database\Eloquent\Model;
         * use Illuminate\Database\Eloquent\Relations\MorphTo;
         * use Illuminate\Database\Eloquent\Relations\MorphOne;
         *
         * 主体模型
         * class Image extends Model {
         *     public function imageable(): MorphTo {
         *         return $this->morphTo();
         *     }
         * }
         * class Post extends Model {
         *     public function image(): MorphOne {
         *         return $this->morphOne(Image::class, 'imageable');
         *     }
         * }
         * class User extends Model {
         *     public function image(): MorphOne {
         *         return $this->morphOne(Image::class, 'imageable');
         *     }
         * }
         * 对于上面的示例 用关系来处理就很方便了 也可以不用写这些 imageable() 和 image() 方法了
         *
         * 'images' => [
         *     // 关系表说明 多对多是中间表 一对一和一对多是主体表
         *     'comment' => '图片表',
         *     'type' => MorphConfigResource::MORPH_ONE2ONE,
         *     // 一对一 和 一对多 是需要相关的主体模型 多对多也需要但是主体有些不一样
         *     'model' => 'Image::class', // '别名/模型:class', // 别名或者模型::class
         *     // 主体在关系表中的字段名 多对多关系用到  为空则为 主体模型.主体模型的主键
         *     'primary_name' => null,
         *     // 关联的ablename 为空则默认关系表名.'able'
         *     'able_name' => 'imageable',
         *     // able_type_name 字段名 如果null 则自动根据able_name 生成 [$(able_name) . '_type']
         *     'able_type_name' => null,
         *     // able_id_name 字段名 如果null 则自动根据able_name 生成 [$(able_name) . '_id']
         *     'able_id_name' => null,
         *      // 主体中定义的的方法名 如果为空则为 关系表名.'able' 多对多关系这个就不需要了
         *     'morphs_name' => 'imageable',
         *     // 关系中定义的方法名 为空则用关系表名
         *     'morph_name' => 'image',
         *     //关系列表 主体对应多个模型
         *     'morphs' => [
         *          // 别名或者模型:class  比如 Post::class 关联
         *          // '别名/模型:class' => '方法名',
         *          // 一对一 一对多 的情况下 方法名会绑定在关系模型中  null 则用 morph_name 覆盖
         *          // 多对多 的情况下 方法名会绑定在主体模型中 null 则用 关系模型.'_'. morph_name 方法
         *         'Post::class' => null, // 去掉 Post::class 外面的单引号
         *         'User::class' => null, // 去掉 User::class 外面的单引号
         *     ],
         * ],
         */

        /**
         * 多态一对多关系 示例
         * 示例表结构
         * posts                  videos                 comments
         *  id - integer            id - integer            id - integer
         *  title - string          title - string          content - string
         *  body - text             url - string            commentable_id  - integer
         *                                                  commentable_type  - string
         * 示例模型结构
         * namespace App\Models;
         * use Illuminate\Database\Eloquent\Model;
         * use Illuminate\Database\Eloquent\Relations\MorphTo;
         * use Illuminate\Database\Eloquent\Relations\MorphMany;
         *
         * 主体模型
         * class Comment extends Model {
         *     public function commentable(): MorphTo {
         *         return $this->morphTo();
         *     }
         * }
         * class Post extends Model {
         *     public function comments(): MorphMany {
         *         return $this->morphMany(Comment::class, 'commentable');
         *     }
         * }
         * class Video extends Model {
         *     public function comments(): MorphMany {
         *         return $this->morphMany(Comment::class, 'commentable');
         *     }
         * }
         * 对于上面的示例 用关系来处理就很方便了 也可以不用写这些 commentable() 和 comments() 方法了
         *
         * 'comments' => [
         *     // 关系表说明 多对多是中间表 一对一和一对多是主体表
         *     'comment' => '评论',
         *     'type' => MorphConfigResource::MORPH_ONE2MANY,
         *     // 一对一 和 一对多 是需要相关的主体模型 多对多也需要但是主体有些不一样
         *     'model' => 'Comment::class', // '别名/模型:class',
         *     // 主体在关系表中的字段名 多对多关系用到  为空则为 主体模型.主体模型的主键
         *     'primary_name' => null,
         *     // 关联的ablename 为空则默认关系表名.'able'
         *     'able_name' => 'commentable',
         *     // able_type_name 字段名 如果null 则自动根据able_name 生成 [$(able_name) . '_type']
         *     'able_type_name' => null,
         *     // able_id_name 字段名 如果null 则自动根据able_name 生成 [$(able_name) . '_id']
         *     'able_id_name' => null,
         *      // 主体中定义的的方法名 如果为空则为 关系表名.'_able' 多对多关系这个就不需要了
         *     'morphs_name' => 'commentable',
         *     // 关系中定义的方法名 为空则用关系表名
         *     'morph_name' => 'comments',
         *     //关系列表 主体对应多个模型
         *     'morphs' => [
         *          // 别名或者模型:class  比如 Post::class 关联
         *          // '别名/模型:class' => '方法名',
         *          // 一对一 一对多 的情况下 方法名会绑定在关系模型中  null 则用 morph_name 覆盖
         *          // 多对多 的情况下 方法名会绑定在主体模型中 null 则用 关系模型.'_'. morph_name 方法
         *         'Post::class' => null, // 去掉 Post::class 外面的单引号
         *         'Video::class' => null, // 去掉 Video::class 外面的单引号
         *     ],
         * ],
         */

        /**
         * 多态多对多关系 示例
         * 示例表结构
         * posts               videos              tags               taggables
         *  id - integer         id - integer         id - integer      tag_id - integer
         *  name - string        name - string        name - string     taggable_id - integer
         *                                                              taggable_type - string
         * 示例模型结构
         * namespace App\Models;
         * use Illuminate\Database\Eloquent\Model;
         * use Illuminate\Database\Eloquent\Relations\MorphToMany;
         *
         * 主体模型
         * class Tag  extends Model {
         *     public function posts(): MorphToMany {
         *         return $this->morphedByMany(Post::class, 'taggable');
         *     }
         *     public function videos(): MorphToMany {
         *         return $this->morphedByMany(Video::class, 'taggable');
         *     }
         * }
         * class Post extends Model {
         *      public function tags(): MorphToMany {
         *          return $this->morphToMany(Tag::class, 'taggable');
         *      }
         * }
         * class Video extends Model {
         *       public function tags(): MorphToMany {
         *           return $this->morphToMany(Tag::class, 'taggable');
         *       }
         * }
         * 对于上面的示例 用关系来处理就很方便了 也可以不用写这些 tags() 和 posts() videos() 方法了
         *
         * 'taggables' => [
         *     // 关系表说明 多对多是中间表 一对一和一对多是主体表
         *     'comment' => '标签',
         *     'type' => MorphConfigResource::MORPH_MANY2MANY,
         *     // 一对一 和 一对多 是需要相关的主体模型 多对多也需要但是主体有些不一样
         *     'model' => 'Tag::class', // '别名/模型:class',
         *     // 主体在关系表中的字段名 多对多关系用到  为空则为 主体模型.主体模型的主键
         *     'primary_name' => 'tag_id',
         *     // 关联的ablename 为空则默认关系表名.'able'
         *     'able_name' => 'taggable',
         *     // able_type_name 字段名 如果null 则自动根据able_name 生成 [$(able_name) . '_type']
         *     'able_type_name' => null,
         *     // able_id_name 字段名 如果null 则自动根据able_name 生成 [$(able_name) . '_id']
         *     'able_id_name' => null,
         *     // 主体中定义的的方法名 如果为空则为 关系表名.'_able' 多对多关系这个就不需要了
         *     'morphs_name' => null,
         *     // 关系中定义的方法名 为空则用关系表名
         *     'morph_name' => 'comments',
         *     //关系列表 主体对应多个模型
         *     'morphs' => [
         *         // 别名或者模型:class  比如 Post::class 关联
         *         // '别名/模型:class' => '方法名',
         *         // 一对一 一对多 的情况下 方法名会绑定在关系模型中  null 则用 morph_name 覆盖
         *         // 多对多 的情况下 方法名会绑定在主体模型中 null 则用 关系模型.'_'. morph_name 方法
         *         'Post::class' => 'posts', // 去掉 Post::class 外面的单引号
         *         'Video::class' => 'videos', // 去掉 Video::class 外面的单引号
         *     ],
         * ],
         */
    ],
];
